@extends('layouts.ui')
@section('breadcrumb')
<a-breadcrumb style="margin: 16px 0">
    <a-breadcrumb-item>{{ __('Home') }}</a-breadcrumb-item>
</a-breadcrumb>
@endsection


@section('content')
{!! $page ? $page->getContent() : '' !!}
<h1>{{ __('AvoRed Special Week') }}</h1>
<a-row :gutter="15">
  @foreach ($products as $product)

  <div class="col s12 m12 l9">
      <div class="col s12 m4 l4">
        <div class="card-content">
        <p>{{$product->name}}</p>
            <span class="card-title text-ellipsis">{{$product->description}}i</span>
            <img src="storagte/$product->product_images->path" class="responsive-img" alt="">
            <div class="row">
              <h5 class="col s12 m12 l8 mt-3">{{ session()->get('default_currency')->symbol }} {{$product->price}}</h5>
              <a class="col s12 m12 l4 mt-2 waves-effect waves-light gradient-45deg-deep-purple-blue btn modal-trigger"
                href="{{route('product.show', $product->slug)}}">View</a>
            </div>

            
            </div> </div>      







      <a-col :span="6" class="product-cards-list">
        @include('category.card.product', ['product' => $product])
      </a-col>
  @endforeach
</a-row>
@endsection
